//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OnlineCourses.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Blocks
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Blocks()
        {
            this.Lessons = new HashSet<Lessons>();
        }
    
        public int BlockID { get; set; }
        public int CourseID { get; set; }
        public string BlockName { get; set; }
        public Nullable<int> NumOfLessons { get; set; }
        public Nullable<double> NumOfHours { get; set; }
        public string BlockDescription { get; set; }
        public string BlockGoalName { get; set; }
    
        public virtual Courses Courses { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Lessons> Lessons { get; set; }
    }
}
