﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineCourses.Models
{
    public class LoginPassword
    {
        [Required(ErrorMessage = "Введите логин")]
        public string UserLogin { get; set; }
        [Required(ErrorMessage = "Введите пароль")]
        public string UserPassword { get; set; }
        

        
        public string UserEmail { get; set; }



        public bool Remember { get; set; }
    }
}