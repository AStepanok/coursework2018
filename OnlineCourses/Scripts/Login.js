﻿$(function() {

    $('#login-form-link').click(function(e) {
		$("#login-form").delay(100).fadeIn(100);
 		$("#register-form").fadeOut(100);
		$('#register-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#register-form-link').click(function(e) {
		$("#register-form").delay(100).fadeIn(100);
 		$("#login-form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});

});

function registerNewUser(FIO, login, email, pas, pasConfirm, orgName) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    if (!($.trim(login) == "" || $.trim(email) == "" || $.trim(pas) == "" || $.trim(pasConfirm) == "" || $.trim(FIO) == "" )) {
       
        if (email.match(re) !== null) {
            if (pas.length >= 8) {
                if (pas == pasConfirm) {
                    $.ajax({
                        url: "/Home/CheckUserLogin?login="+login,
                        type: "POST",
                        
                        dataType: 'json',
                        contentType: false,
                        processData: false,
                        success: function (response) {
                            if (response.success) {
                                $.ajax({
                                    url: "/Home/CheckUserEmail?email=" + email,
                                    type: "POST",

                                    dataType: 'json',
                                    contentType: false,
                                    processData: false,
                                    success: function (response) {
                                        if (response.success) {
                                            $.ajax({
                                                url: "/Home/Register",
                                                type: "POST",
                                                data: { FIO: FIO, login: login, email: email, pas: pas, orgName: orgName },
                                                success: function () {

                                                    toastr['success']('Пользователь успешно создан! Можете войти в систему! :)');


                                                    $('#login-form-link').click();
                                                }, error: function (xhr, ajaxOptions, thrownError) {
                                                    alert(thrownError.message);
                                                }

                                            });
                                        } else {
                                            // DoSomethingElse()
                                            toastr['error']('Такой email уже привязан или он имел неверный формат!');
                                        }
                                    },
                                    error: function (response) {
                                        toastr['error']('Такой email уже привязан или он имел неверный формат!');
                                    }



                                });
                            } else {
                                // DoSomethingElse()
                                toastr['error']('Такой логин уже существует или он имел неверный формат!');
                            }
                        },
                        error: function (response) {
                            toastr['error']('Такой логин уже существует!');
                        }

                        

                    });
                } else {
                    toastr['error']('Пароли не совпадают!');
                }
            } else {
                toastr['error']('Длина пароля должна быть не меньше 8 символов!');
            }
        } else {
            toastr['error']('Email имеет неверный формат!');
        }
    } else {
        toastr['error']('Все поля должны быть заполнены!');
    }

}

function hey() {
    alert("HEY!");
}