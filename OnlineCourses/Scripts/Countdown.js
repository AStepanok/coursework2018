﻿


var wrapper, wrapperHW;

var countDownDate, countDownDateHW;


$(document).ready(function () {

    
    
    wrapper = $("#countdown");
    
    if (wrapper != null) {
        var str = wrapper.attr("class");
       
        if (str != null) {
            var matches = wrapper.attr("class").split('.');
            
            countDownDate = new Date(matches[2], matches[0] - 1, matches[1], matches[3], matches[4]);
        }
    }
    
    

    wrapperHW = $("#countdownHW");

    if (wrapperHW != null) {
        var str = wrapperHW.attr("class");
        if (str != null) {
            matches = wrapperHW.attr("class").split('.');
            countDownDateHW = new Date(matches[2], matches[0] - 1, matches[1], matches[3], matches[4]);
        }
    }

    
    
});



// Update the count down every 1 second
var x = setInterval(function() {

  // Get todays date and time
  var now = new Date().getTime();

  // Find the distance between now an the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
 

  // Display the result in the element with id="demo"
  document.getElementById("countdown").innerHTML = days + " дн. " + hours + " ч. "
  + minutes + " мин. ";

  // If the count down is finished, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("countdown").innerHTML = "Лекция началась!";
  }
}, 10);

// Update the count down every 1 second
var y = setInterval(function () {

    // Get todays date and time
    var now = new Date().getTime();

    // Find the distance between now an the count down date
    var distance = countDownDateHW - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    

    // Display the result in the element with id="demo"
    document.getElementById("countdownHW").innerHTML = days + " дн. " + hours + " ч. "
        + minutes + " мин. ";

    // If the count down is finished, write some text 
    if (distance < 0) {
        clearInterval(y);
        document.getElementById("countdownHW").innerHTML = "Дедлайн прошел!";
    }
}, 10);