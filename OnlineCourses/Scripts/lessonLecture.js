﻿function showLecture(source) {
    $('.lessonLecture').show();
    
    $('.lessonLecture > iframe').attr('src', source);
};

function closeLecture() {
    $('.lessonLecture').hide();

    $('.lessonLecture > iframe').attr('src', $('.lessonLecture > iframe').attr('src'));
};
