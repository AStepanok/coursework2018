
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 10/15/2017 15:30:24
-- Generated from EDMX file: C:\Users\Антон Степанок\Desktop\OnlineCourses-master\OnlineCourses\Models\EveningSchoolsDBModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [u0394083_eveningshoolsDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[u0394083_esAdmin].[FK_Blocks_Courses]', 'F') IS NOT NULL
    ALTER TABLE [u0394083_esAdmin].[Blocks] DROP CONSTRAINT [FK_Blocks_Courses];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[FK_Courses_Organizations]', 'F') IS NOT NULL
    ALTER TABLE [u0394083_esAdmin].[Courses] DROP CONSTRAINT [FK_Courses_Organizations];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[FK_Homeworks_Lessons]', 'F') IS NOT NULL
    ALTER TABLE [u0394083_esAdmin].[Homeworks] DROP CONSTRAINT [FK_Homeworks_Lessons];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[FK_Lessons_Blocks]', 'F') IS NOT NULL
    ALTER TABLE [u0394083_esAdmin].[Lessons] DROP CONSTRAINT [FK_Lessons_Blocks];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[FK_Reviews_Courses]', 'F') IS NOT NULL
    ALTER TABLE [u0394083_esAdmin].[Reviews] DROP CONSTRAINT [FK_Reviews_Courses];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[FK_Stages_Organizations]', 'F') IS NOT NULL
    ALTER TABLE [u0394083_esAdmin].[Stages] DROP CONSTRAINT [FK_Stages_Organizations];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[FK_Teachers_Organizations]', 'F') IS NOT NULL
    ALTER TABLE [u0394083_esAdmin].[Teachers] DROP CONSTRAINT [FK_Teachers_Organizations];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[FK_TeachersCourses_Courses]', 'F') IS NOT NULL
    ALTER TABLE [u0394083_esAdmin].[TeachersCourses] DROP CONSTRAINT [FK_TeachersCourses_Courses];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[FK_TeachersCourses_Teachers]', 'F') IS NOT NULL
    ALTER TABLE [u0394083_esAdmin].[TeachersCourses] DROP CONSTRAINT [FK_TeachersCourses_Teachers];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[FK_TeachersLessons_Lessons]', 'F') IS NOT NULL
    ALTER TABLE [u0394083_esAdmin].[TeachersLessons] DROP CONSTRAINT [FK_TeachersLessons_Lessons];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[FK_TeachersLessons_Teachers]', 'F') IS NOT NULL
    ALTER TABLE [u0394083_esAdmin].[TeachersLessons] DROP CONSTRAINT [FK_TeachersLessons_Teachers];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[FK_Users_Organizations]', 'F') IS NOT NULL
    ALTER TABLE [u0394083_esAdmin].[Users] DROP CONSTRAINT [FK_Users_Organizations];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[FK_UsersCourses_Courses]', 'F') IS NOT NULL
    ALTER TABLE [u0394083_esAdmin].[UsersCourses] DROP CONSTRAINT [FK_UsersCourses_Courses];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[FK_UsersCourses_Users]', 'F') IS NOT NULL
    ALTER TABLE [u0394083_esAdmin].[UsersCourses] DROP CONSTRAINT [FK_UsersCourses_Users];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[FK_UsersHomeworksTeachers_Homeworks]', 'F') IS NOT NULL
    ALTER TABLE [u0394083_esAdmin].[UsersHomeworksTeachers] DROP CONSTRAINT [FK_UsersHomeworksTeachers_Homeworks];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[FK_UsersHomeworksTeachers_Teachers]', 'F') IS NOT NULL
    ALTER TABLE [u0394083_esAdmin].[UsersHomeworksTeachers] DROP CONSTRAINT [FK_UsersHomeworksTeachers_Teachers];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[FK_UsersHomeworksTeachers_Users]', 'F') IS NOT NULL
    ALTER TABLE [u0394083_esAdmin].[UsersHomeworksTeachers] DROP CONSTRAINT [FK_UsersHomeworksTeachers_Users];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[u0394083_esAdmin].[Blocks]', 'U') IS NOT NULL
    DROP TABLE [u0394083_esAdmin].[Blocks];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[Courses]', 'U') IS NOT NULL
    DROP TABLE [u0394083_esAdmin].[Courses];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[Homeworks]', 'U') IS NOT NULL
    DROP TABLE [u0394083_esAdmin].[Homeworks];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[Lessons]', 'U') IS NOT NULL
    DROP TABLE [u0394083_esAdmin].[Lessons];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[Organizations]', 'U') IS NOT NULL
    DROP TABLE [u0394083_esAdmin].[Organizations];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[Reviews]', 'U') IS NOT NULL
    DROP TABLE [u0394083_esAdmin].[Reviews];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[Stages]', 'U') IS NOT NULL
    DROP TABLE [u0394083_esAdmin].[Stages];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[Teachers]', 'U') IS NOT NULL
    DROP TABLE [u0394083_esAdmin].[Teachers];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[TeachersCourses]', 'U') IS NOT NULL
    DROP TABLE [u0394083_esAdmin].[TeachersCourses];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[TeachersLessons]', 'U') IS NOT NULL
    DROP TABLE [u0394083_esAdmin].[TeachersLessons];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[Users]', 'U') IS NOT NULL
    DROP TABLE [u0394083_esAdmin].[Users];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[UsersCourses]', 'U') IS NOT NULL
    DROP TABLE [u0394083_esAdmin].[UsersCourses];
GO
IF OBJECT_ID(N'[u0394083_esAdmin].[UsersHomeworksTeachers]', 'U') IS NOT NULL
    DROP TABLE [u0394083_esAdmin].[UsersHomeworksTeachers];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Blocks'
CREATE TABLE [dbo].[Blocks] (
    [BlockID] int IDENTITY(1,1) NOT NULL,
    [CourseID] int  NOT NULL,
    [BlockName] nvarchar(100)  NOT NULL,
    [NumOfLessons] int  NULL,
    [NumOfHours] float  NULL,
    [BlockDescription] nvarchar(1000)  NOT NULL,
    [BlockGoalName] nvarchar(100)  NOT NULL
);
GO

-- Creating table 'Courses'
CREATE TABLE [dbo].[Courses] (
    [CourseID] int IDENTITY(1,1) NOT NULL,
    [OrganizationID] int  NOT NULL,
    [CourseName] nvarchar(100)  NOT NULL,
    [CoursePrice] int  NOT NULL,
    [CourseDiscountPrice] int  NULL,
    [CourseDiscountDeadline] datetime  NULL,
    [CourseImage] nvarchar(250)  NOT NULL,
    [CourseBackground] nvarchar(100)  NULL,
    [CourseDescription] nvarchar(4000)  NULL
);
GO

-- Creating table 'Homeworks'
CREATE TABLE [dbo].[Homeworks] (
    [HomeworkID] int IDENTITY(1,1) NOT NULL,
    [LessonID] int  NOT NULL,
    [HomeworkName] nvarchar(150)  NOT NULL,
    [HomeworkDeadline] datetime  NOT NULL,
    [HomeworkGoal] nvarchar(1000)  NOT NULL,
    [HomeworkTask] nvarchar(4000)  NOT NULL
);
GO

-- Creating table 'Lessons'
CREATE TABLE [dbo].[Lessons] (
    [LessonID] int IDENTITY(1,1) NOT NULL,
    [BlockID] int  NOT NULL,
    [LessonName] nvarchar(250)  NOT NULL,
    [LessonImage] nvarchar(250)  NOT NULL,
    [LessonLecture] nvarchar(250)  NOT NULL,
    [LessonDuration] time  NOT NULL,
    [LessonDateTime] datetime  NOT NULL,
    [LessonDescription] nvarchar(1000)  NOT NULL
);
GO

-- Creating table 'Organizations'
CREATE TABLE [dbo].[Organizations] (
    [OrganizationID] int IDENTITY(1,1) NOT NULL,
    [OrganizationName] nvarchar(250)  NOT NULL,
    [OrganizationShortName] nvarchar(50)  NULL,
    [OrganizationLogin] nvarchar(250)  NOT NULL,
    [OrganizationPassword] nvarchar(250)  NOT NULL,
    [OrganizationLogo] nvarchar(250)  NULL
);
GO

-- Creating table 'Reviews'
CREATE TABLE [dbo].[Reviews] (
    [ReviewID] int IDENTITY(1,1) NOT NULL,
    [CourseID] int  NOT NULL,
    [ReviewName] nvarchar(150)  NOT NULL,
    [ReviewDescription] nvarchar(500)  NOT NULL,
    [ReviewImage] nvarchar(250)  NOT NULL
);
GO

-- Creating table 'Stages'
CREATE TABLE [dbo].[Stages] (
    [StageID] int IDENTITY(1,1) NOT NULL,
    [OrganizationID] int  NOT NULL,
    [StageDate] datetime  NOT NULL,
    [StageName] nvarchar(50)  NOT NULL,
    [StageDescription] nvarchar(1000)  NOT NULL,
    [StageImage] nvarchar(250)  NOT NULL
);
GO

-- Creating table 'Teachers'
CREATE TABLE [dbo].[Teachers] (
    [TeacherID] int IDENTITY(1,1) NOT NULL,
    [OrganizationID] int  NOT NULL,
    [TeacherName] nvarchar(250)  NOT NULL,
    [TeacherPosition] nvarchar(250)  NOT NULL,
    [TeacherImage] nvarchar(250)  NOT NULL,
    [TeacherDescriprion] nvarchar(500)  NOT NULL,
    [TeacherLogin] nvarchar(250)  NOT NULL,
    [TeacherPassword] nvarchar(250)  NOT NULL,
    [TeacherEmail] nvarchar(200)  NULL,
    [TeacherPhone] nvarchar(100)  NULL,
    [TeacherVK] nvarchar(100)  NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [UserID] int IDENTITY(1,1) NOT NULL,
    [OrganizationID] int  NOT NULL,
    [UserLogin] nvarchar(100)  NOT NULL,
    [UserPassword] nvarchar(100)  NOT NULL,
    [UserName] nvarchar(100)  NOT NULL,
    [UserImage] nvarchar(150)  NULL,
    [UserEmail] nvarchar(200)  NULL,
    [UserPhone] nvarchar(100)  NULL,
    [UserVK] nvarchar(200)  NULL
);
GO

-- Creating table 'UsersHomeworksTeachers'
CREATE TABLE [dbo].[UsersHomeworksTeachers] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [UserID] int  NOT NULL,
    [HomeworkID] int  NOT NULL,
    [TeacherID] int  NULL,
    [Mark] int  NULL,
    [TeacherComment] nvarchar(4000)  NULL,
    [UserComment] nvarchar(4000)  NULL,
    [TeacherName] nvarchar(200)  NULL
);
GO

-- Creating table 'TeachersCourses'
CREATE TABLE [dbo].[TeachersCourses] (
    [Courses_CourseID] int  NOT NULL,
    [Teachers_TeacherID] int  NOT NULL
);
GO

-- Creating table 'TeachersLessons'
CREATE TABLE [dbo].[TeachersLessons] (
    [Lessons_LessonID] int  NOT NULL,
    [Teachers_TeacherID] int  NOT NULL
);
GO

-- Creating table 'UsersCourses'
CREATE TABLE [dbo].[UsersCourses] (
    [Courses_CourseID] int  NOT NULL,
    [Users_UserID] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [BlockID] in table 'Blocks'
ALTER TABLE [dbo].[Blocks]
ADD CONSTRAINT [PK_Blocks]
    PRIMARY KEY CLUSTERED ([BlockID] ASC);
GO

-- Creating primary key on [CourseID] in table 'Courses'
ALTER TABLE [dbo].[Courses]
ADD CONSTRAINT [PK_Courses]
    PRIMARY KEY CLUSTERED ([CourseID] ASC);
GO

-- Creating primary key on [HomeworkID] in table 'Homeworks'
ALTER TABLE [dbo].[Homeworks]
ADD CONSTRAINT [PK_Homeworks]
    PRIMARY KEY CLUSTERED ([HomeworkID] ASC);
GO

-- Creating primary key on [LessonID] in table 'Lessons'
ALTER TABLE [dbo].[Lessons]
ADD CONSTRAINT [PK_Lessons]
    PRIMARY KEY CLUSTERED ([LessonID] ASC);
GO

-- Creating primary key on [OrganizationID] in table 'Organizations'
ALTER TABLE [dbo].[Organizations]
ADD CONSTRAINT [PK_Organizations]
    PRIMARY KEY CLUSTERED ([OrganizationID] ASC);
GO

-- Creating primary key on [ReviewID] in table 'Reviews'
ALTER TABLE [dbo].[Reviews]
ADD CONSTRAINT [PK_Reviews]
    PRIMARY KEY CLUSTERED ([ReviewID] ASC);
GO

-- Creating primary key on [StageID] in table 'Stages'
ALTER TABLE [dbo].[Stages]
ADD CONSTRAINT [PK_Stages]
    PRIMARY KEY CLUSTERED ([StageID] ASC);
GO

-- Creating primary key on [TeacherID] in table 'Teachers'
ALTER TABLE [dbo].[Teachers]
ADD CONSTRAINT [PK_Teachers]
    PRIMARY KEY CLUSTERED ([TeacherID] ASC);
GO

-- Creating primary key on [UserID] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([UserID] ASC);
GO

-- Creating primary key on [ID], [UserID], [HomeworkID] in table 'UsersHomeworksTeachers'
ALTER TABLE [dbo].[UsersHomeworksTeachers]
ADD CONSTRAINT [PK_UsersHomeworksTeachers]
    PRIMARY KEY CLUSTERED ([ID], [UserID], [HomeworkID] ASC);
GO

-- Creating primary key on [Courses_CourseID], [Teachers_TeacherID] in table 'TeachersCourses'
ALTER TABLE [dbo].[TeachersCourses]
ADD CONSTRAINT [PK_TeachersCourses]
    PRIMARY KEY CLUSTERED ([Courses_CourseID], [Teachers_TeacherID] ASC);
GO

-- Creating primary key on [Lessons_LessonID], [Teachers_TeacherID] in table 'TeachersLessons'
ALTER TABLE [dbo].[TeachersLessons]
ADD CONSTRAINT [PK_TeachersLessons]
    PRIMARY KEY CLUSTERED ([Lessons_LessonID], [Teachers_TeacherID] ASC);
GO

-- Creating primary key on [Courses_CourseID], [Users_UserID] in table 'UsersCourses'
ALTER TABLE [dbo].[UsersCourses]
ADD CONSTRAINT [PK_UsersCourses]
    PRIMARY KEY CLUSTERED ([Courses_CourseID], [Users_UserID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [CourseID] in table 'Blocks'
ALTER TABLE [dbo].[Blocks]
ADD CONSTRAINT [FK_Blocks_Courses]
    FOREIGN KEY ([CourseID])
    REFERENCES [dbo].[Courses]
        ([CourseID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Blocks_Courses'
CREATE INDEX [IX_FK_Blocks_Courses]
ON [dbo].[Blocks]
    ([CourseID]);
GO

-- Creating foreign key on [BlockID] in table 'Lessons'
ALTER TABLE [dbo].[Lessons]
ADD CONSTRAINT [FK_Lessons_Blocks]
    FOREIGN KEY ([BlockID])
    REFERENCES [dbo].[Blocks]
        ([BlockID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Lessons_Blocks'
CREATE INDEX [IX_FK_Lessons_Blocks]
ON [dbo].[Lessons]
    ([BlockID]);
GO

-- Creating foreign key on [OrganizationID] in table 'Courses'
ALTER TABLE [dbo].[Courses]
ADD CONSTRAINT [FK_Courses_Organizations]
    FOREIGN KEY ([OrganizationID])
    REFERENCES [dbo].[Organizations]
        ([OrganizationID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Courses_Organizations'
CREATE INDEX [IX_FK_Courses_Organizations]
ON [dbo].[Courses]
    ([OrganizationID]);
GO

-- Creating foreign key on [CourseID] in table 'Reviews'
ALTER TABLE [dbo].[Reviews]
ADD CONSTRAINT [FK_Reviews_Courses]
    FOREIGN KEY ([CourseID])
    REFERENCES [dbo].[Courses]
        ([CourseID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Reviews_Courses'
CREATE INDEX [IX_FK_Reviews_Courses]
ON [dbo].[Reviews]
    ([CourseID]);
GO

-- Creating foreign key on [LessonID] in table 'Homeworks'
ALTER TABLE [dbo].[Homeworks]
ADD CONSTRAINT [FK_Homeworks_Lessons]
    FOREIGN KEY ([LessonID])
    REFERENCES [dbo].[Lessons]
        ([LessonID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Homeworks_Lessons'
CREATE INDEX [IX_FK_Homeworks_Lessons]
ON [dbo].[Homeworks]
    ([LessonID]);
GO

-- Creating foreign key on [HomeworkID] in table 'UsersHomeworksTeachers'
ALTER TABLE [dbo].[UsersHomeworksTeachers]
ADD CONSTRAINT [FK_UsersHomeworksTeachers_Homeworks]
    FOREIGN KEY ([HomeworkID])
    REFERENCES [dbo].[Homeworks]
        ([HomeworkID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UsersHomeworksTeachers_Homeworks'
CREATE INDEX [IX_FK_UsersHomeworksTeachers_Homeworks]
ON [dbo].[UsersHomeworksTeachers]
    ([HomeworkID]);
GO

-- Creating foreign key on [OrganizationID] in table 'Stages'
ALTER TABLE [dbo].[Stages]
ADD CONSTRAINT [FK_Stages_Organizations]
    FOREIGN KEY ([OrganizationID])
    REFERENCES [dbo].[Organizations]
        ([OrganizationID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Stages_Organizations'
CREATE INDEX [IX_FK_Stages_Organizations]
ON [dbo].[Stages]
    ([OrganizationID]);
GO

-- Creating foreign key on [OrganizationID] in table 'Teachers'
ALTER TABLE [dbo].[Teachers]
ADD CONSTRAINT [FK_Teachers_Organizations]
    FOREIGN KEY ([OrganizationID])
    REFERENCES [dbo].[Organizations]
        ([OrganizationID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Teachers_Organizations'
CREATE INDEX [IX_FK_Teachers_Organizations]
ON [dbo].[Teachers]
    ([OrganizationID]);
GO

-- Creating foreign key on [OrganizationID] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [FK_Users_Organizations]
    FOREIGN KEY ([OrganizationID])
    REFERENCES [dbo].[Organizations]
        ([OrganizationID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Users_Organizations'
CREATE INDEX [IX_FK_Users_Organizations]
ON [dbo].[Users]
    ([OrganizationID]);
GO

-- Creating foreign key on [TeacherID] in table 'UsersHomeworksTeachers'
ALTER TABLE [dbo].[UsersHomeworksTeachers]
ADD CONSTRAINT [FK_UsersHomeworksTeachers_Teachers]
    FOREIGN KEY ([TeacherID])
    REFERENCES [dbo].[Teachers]
        ([TeacherID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UsersHomeworksTeachers_Teachers'
CREATE INDEX [IX_FK_UsersHomeworksTeachers_Teachers]
ON [dbo].[UsersHomeworksTeachers]
    ([TeacherID]);
GO

-- Creating foreign key on [UserID] in table 'UsersHomeworksTeachers'
ALTER TABLE [dbo].[UsersHomeworksTeachers]
ADD CONSTRAINT [FK_UsersHomeworksTeachers_Users]
    FOREIGN KEY ([UserID])
    REFERENCES [dbo].[Users]
        ([UserID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UsersHomeworksTeachers_Users'
CREATE INDEX [IX_FK_UsersHomeworksTeachers_Users]
ON [dbo].[UsersHomeworksTeachers]
    ([UserID]);
GO

-- Creating foreign key on [Courses_CourseID] in table 'TeachersCourses'
ALTER TABLE [dbo].[TeachersCourses]
ADD CONSTRAINT [FK_TeachersCourses_Courses]
    FOREIGN KEY ([Courses_CourseID])
    REFERENCES [dbo].[Courses]
        ([CourseID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Teachers_TeacherID] in table 'TeachersCourses'
ALTER TABLE [dbo].[TeachersCourses]
ADD CONSTRAINT [FK_TeachersCourses_Teachers]
    FOREIGN KEY ([Teachers_TeacherID])
    REFERENCES [dbo].[Teachers]
        ([TeacherID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TeachersCourses_Teachers'
CREATE INDEX [IX_FK_TeachersCourses_Teachers]
ON [dbo].[TeachersCourses]
    ([Teachers_TeacherID]);
GO

-- Creating foreign key on [Lessons_LessonID] in table 'TeachersLessons'
ALTER TABLE [dbo].[TeachersLessons]
ADD CONSTRAINT [FK_TeachersLessons_Lessons]
    FOREIGN KEY ([Lessons_LessonID])
    REFERENCES [dbo].[Lessons]
        ([LessonID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Teachers_TeacherID] in table 'TeachersLessons'
ALTER TABLE [dbo].[TeachersLessons]
ADD CONSTRAINT [FK_TeachersLessons_Teachers]
    FOREIGN KEY ([Teachers_TeacherID])
    REFERENCES [dbo].[Teachers]
        ([TeacherID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TeachersLessons_Teachers'
CREATE INDEX [IX_FK_TeachersLessons_Teachers]
ON [dbo].[TeachersLessons]
    ([Teachers_TeacherID]);
GO

-- Creating foreign key on [Courses_CourseID] in table 'UsersCourses'
ALTER TABLE [dbo].[UsersCourses]
ADD CONSTRAINT [FK_UsersCourses_Courses]
    FOREIGN KEY ([Courses_CourseID])
    REFERENCES [dbo].[Courses]
        ([CourseID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Users_UserID] in table 'UsersCourses'
ALTER TABLE [dbo].[UsersCourses]
ADD CONSTRAINT [FK_UsersCourses_Users]
    FOREIGN KEY ([Users_UserID])
    REFERENCES [dbo].[Users]
        ([UserID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UsersCourses_Users'
CREATE INDEX [IX_FK_UsersCourses_Users]
ON [dbo].[UsersCourses]
    ([Users_UserID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------