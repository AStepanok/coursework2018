﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineCourses.Models;
using System.Web.Security;
using System.Data.Entity.Validation;
using System.IO;
using System.Security.Cryptography;

namespace OnlineCourses.Controllers
{
    public class StudentController : Controller
    {
        coursesDBEntities _database = new coursesDBEntities();


        // GET: CourseMaterials
        public ActionResult Index()
        {
            Users u = _database.Users.FirstOrDefault(x => x.UserLogin == User.Identity.Name);

            

            return View(u);
        }

        

        

        public ActionResult ViewSingleCourse(int? id)
        {
            Courses c = _database.Courses.FirstOrDefault(x => x.CourseID == id);
            return View(c);
        }

        public ActionResult ViewUnboughtCourses()
        {
            Users u = _database.Users.FirstOrDefault(x => x.UserLogin == User.Identity.Name);
            return View(u);
        }

        public ActionResult ViewSingleLesson(int? id)
        {
            Lessons l = _database.Lessons.FirstOrDefault(x => x.LessonID == id);
            Blocks block = _database.Blocks.FirstOrDefault(x => x.BlockID == l.BlockID);
            Courses course = _database.Courses.FirstOrDefault(x => x.CourseID == block.CourseID);
            Users creator = _database.Users.FirstOrDefault(x => x.UserID == course.UserID);

            Users user = _database.Users.FirstOrDefault(x => x.UserLogin == User.Identity.Name);

            Directory.CreateDirectory(Server.MapPath("~/Data/") + creator.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + l.LessonID + "/LessonData/");
            string[] fileNames = Directory.GetFiles(Server.MapPath("~/Data/") + creator.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + l.LessonID + "/LessonData/");

            ViewBag.LessonFiles = fileNames;

            foreach (Homeworks hw in l.Homeworks)
            {
                Directory.CreateDirectory(Server.MapPath("~/Data/") + creator.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + l.LessonID + "/" + hw.HomeworkID + "/" + user.UserLogin + "/");
                fileNames = Directory.GetFiles(Server.MapPath("~/Data/") + creator.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + l.LessonID + "/" + hw.HomeworkID + "/" + user.UserLogin + "/");

                ViewBag.HWFiles = fileNames;

                Directory.CreateDirectory(Server.MapPath("~/Data/") + creator.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + l.LessonID + "/" + hw.HomeworkID + "/" + user.UserLogin + "/TeacherAnswer/");
                fileNames = Directory.GetFiles(Server.MapPath("~/Data/") + creator.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + l.LessonID + "/" + hw.HomeworkID + "/" + user.UserLogin + "/TeacherAnswer/");

                ViewBag.TeacherAnswerFiles = fileNames;
            }





            return View(l);
        }

        public ActionResult DeleteHWFile(string filePath, int? id)
        {
            System.IO.File.Delete(filePath);
            return RedirectToAction("ViewSingleLesson", new { id = id }); ;
        }

        public ActionResult ViewTeacherProfile(int? id, int? lID)
        {
            ViewBag.LessonID = lID;
            Users u = _database.Users.FirstOrDefault(x => x.UserID == id);
            return View(u);
        }

        [HttpPost]
        public ActionResult UploadHWFile(int? ID)
        {

            Users user = _database.Users.FirstOrDefault(x => x.UserLogin == User.Identity.Name);
            Homeworks hw = _database.Homeworks.FirstOrDefault(x => x.HomeworkID == ID);
            UsersHomeworksTeachers hwut = _database.UsersHomeworksTeachers.FirstOrDefault(x => x.UserID == user.UserID && x.HomeworkID == hw.HomeworkID);

            if (hwut == null)
            {
                hwut = new UsersHomeworksTeachers();
                hwut.HomeworkID = hw.HomeworkID;
                hwut.UserID = user.UserID;

                _database.UsersHomeworksTeachers.Add(hwut);

                _database.SaveChanges();
            }

            Lessons lesson = _database.Lessons.FirstOrDefault(x => x.LessonID == hw.LessonID);
            Blocks block = _database.Blocks.FirstOrDefault(x => x.BlockID == lesson.BlockID);
            Courses course = _database.Courses.FirstOrDefault(x => x.CourseID == block.CourseID);
            Users creator = _database.Users.FirstOrDefault(x => x.UserID == course.UserID);

            foreach (string s in Request.Files)
            {
                HttpPostedFileBase file = Request.Files[s];
                //  int fileSizeInBytes = file.ContentLength;
                string fileName = file.FileName;
                string fileExtension = file.ContentType;

                if (!string.IsNullOrEmpty(fileName))
                {
                    fileExtension = Path.GetExtension(fileName);
                    Directory.CreateDirectory(Server.MapPath("~/Data/") + creator.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + lesson.LessonID + "/" + hw.HomeworkID + "/" + user.UserLogin + "/");
                    string pathToSave_100 = Server.MapPath("~/Data/") + creator.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + lesson.LessonID + "/" + hw.HomeworkID + "/" + user.UserLogin + "/" + file.FileName;
                    file.SaveAs(pathToSave_100);
                }

            }

            return RedirectToAction("ViewSingleLesson", new { id = lesson.LessonID }); ;

        }

        [HttpPost]
        public void UserSendsHWComment(int? uID, int? hwID, string comment)
        {
            Users user = _database.Users.FirstOrDefault(x => x.UserID == uID);
            Homeworks hw = _database.Homeworks.FirstOrDefault(x => x.HomeworkID == hwID);
            UsersHomeworksTeachers hwut = _database.UsersHomeworksTeachers.FirstOrDefault(x => x.UserID == user.UserID && x.HomeworkID == hw.HomeworkID);

            if (hwut == null)
            {
                hwut = new UsersHomeworksTeachers();
                hwut.HomeworkID = hw.HomeworkID;
                hwut.UserID = user.UserID;

                _database.UsersHomeworksTeachers.Add(hwut);

                _database.SaveChanges();
            }

            hwut.UserComment = comment;

            _database.SaveChanges();

        }

        

        public FileResult DownloadFile(string filePath)
        {
            FileInfo file = new FileInfo(filePath);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, file.Name);
        }

        

        public ActionResult GetAccessToCourse(int? cID)
        {
            Users u = _database.Users.FirstOrDefault(x => x.UserLogin == User.Identity.Name);

            Courses c = _database.Courses.FirstOrDefault(x => x.CourseID == cID);

            u.BoughtCourses.Add(c);

            _database.SaveChanges();

            return RedirectToAction("Index");
        }

        
    }
}