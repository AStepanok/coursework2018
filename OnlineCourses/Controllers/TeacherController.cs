﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using OnlineCourses.Models;
using System.IO;
using System.Security.Cryptography;

namespace OnlineCourses.Controllers
{
    [Authorize(Roles = "user")]
    public class TeacherController : Controller
    {

        coursesDBEntities _database = new coursesDBEntities();


        

        
        public ActionResult ViewBlocks(int? id)
        {
            Courses course = _database.Courses.FirstOrDefault(x => x.CourseID == id);
            return View(course.Blocks);
        }

        public ActionResult ViewLessons(int? id)
        {
            Blocks block = _database.Blocks.FirstOrDefault(x => x.BlockID == id);
            return View(block.Lessons);
        }

        public ActionResult ViewLessonsFullPage(int? id)
        {
            Blocks block = _database.Blocks.FirstOrDefault(x => x.BlockID == id);
            return View(block.Lessons);
        }

        public ActionResult ViewHomeworks(int? id)
        {
            Lessons lesson = _database.Lessons.FirstOrDefault(x => x.LessonID == id);
            return View(lesson.Homeworks);
        }

        public ActionResult ViewHomeworksFullPage(int? id)
        {
            Lessons lesson = _database.Lessons.FirstOrDefault(x => x.LessonID == id);
            return View(lesson.Homeworks);
        }

        public ActionResult ViewHomeworksUsersTeachers(int? id)
        {
            Homeworks hw = _database.Homeworks.FirstOrDefault(x => x.HomeworkID == id);
            return View(hw.UsersHomeworksTeachers);
        }

        public ActionResult ViewHomeworksUsersTeachersFullPage(int? id)
        {
            Homeworks hw = _database.Homeworks.FirstOrDefault(x => x.HomeworkID == id);
            ViewBag.HomeworkID = hw.HomeworkID;
            return View(hw.UsersHomeworksTeachers);
        }

        public ActionResult TeacherGetsTheHW(int? teacherID, int? hwutID)
        {
            Users teahcer = _database.Users.FirstOrDefault(x => x.UserID == teacherID);
            UsersHomeworksTeachers hwut = _database.UsersHomeworksTeachers.FirstOrDefault(x => x.ID == hwutID);

            hwut.TeacherID = teahcer.UserID;
            hwut.TeacherName = teahcer.UserName;

            _database.SaveChanges();

            return RedirectToAction("ViewHomeworksUsersTeachersFullPage", new { id = hwut.HomeworkID });
        }

        public ActionResult TeacherRemoveTheHW(int? hwutID)
        {
            UsersHomeworksTeachers hwut = _database.UsersHomeworksTeachers.FirstOrDefault(x => x.ID == hwutID);
            Users user = _database.Users.FirstOrDefault(x => x.UserID == hwut.UserID);
            Homeworks hw = _database.Homeworks.FirstOrDefault(x => x.HomeworkID == hwut.HomeworkID);
            Lessons lesson = _database.Lessons.FirstOrDefault(x => x.LessonID == hw.LessonID);
            Blocks block = _database.Blocks.FirstOrDefault(x => x.BlockID == lesson.BlockID);
            Courses course = _database.Courses.FirstOrDefault(x => x.CourseID == block.CourseID);
            Users creator = _database.Users.FirstOrDefault(x => x.UserID == course.UserID);

            hwut.TeacherID = null;
            hwut.Mark = null;
            hwut.TeacherComment = null;
            hwut.TeacherName = null;

            _database.SaveChanges();

            string[] teacherFileNames = Directory.GetFiles(Server.MapPath("~/Data/") + creator.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + lesson.LessonID + "/" + hw.HomeworkID + "/" + user.UserLogin + "/TeacherAnswer/");

            foreach (string path in teacherFileNames)
                System.IO.File.Delete(path);

            return RedirectToAction("ViewHomeworksUsersUsersFullPage", new { id = hwut.HomeworkID });
        }

        [HttpGet]
        public ActionResult CheckHW(int? hwutID)
        {
            UsersHomeworksTeachers hwut = _database.UsersHomeworksTeachers.FirstOrDefault(x => x.ID == hwutID);
            Users user = _database.Users.FirstOrDefault(x => x.UserID == hwut.UserID);
            Homeworks hw = _database.Homeworks.FirstOrDefault(x => x.HomeworkID == hwut.HomeworkID);
            Lessons lesson = _database.Lessons.FirstOrDefault(x => x.LessonID == hw.LessonID);
            Blocks block = _database.Blocks.FirstOrDefault(x => x.BlockID == lesson.BlockID);
            Courses course = _database.Courses.FirstOrDefault(x => x.CourseID == block.CourseID);
            Users creator = _database.Users.FirstOrDefault(x => x.UserID == course.UserID);

            string[] fileNames = Directory.GetFiles(Server.MapPath("~/Data/") + creator.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + lesson.LessonID + "/" + hw.HomeworkID + "/" + user.UserLogin);
            string[] teacherFileNames = Directory.GetFiles(Server.MapPath("~/Data/") + creator.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + lesson.LessonID + "/" + hw.HomeworkID + "/" + user.UserLogin + "/TeacherAnswer/");

            ViewBag.Files = fileNames;
            ViewBag.Answers = teacherFileNames;

            return View(hwut);
        }

        [HttpGet]
        public ActionResult CheckHWFullPage(int? hwutID)
        {
            UsersHomeworksTeachers hwut = _database.UsersHomeworksTeachers.FirstOrDefault(x => x.ID == hwutID);
            Users user = _database.Users.FirstOrDefault(x => x.UserID == hwut.UserID);
            Homeworks hw = _database.Homeworks.FirstOrDefault(x => x.HomeworkID == hwut.HomeworkID);
            Lessons lesson = _database.Lessons.FirstOrDefault(x => x.LessonID == hw.LessonID);
            Blocks block = _database.Blocks.FirstOrDefault(x => x.BlockID == lesson.BlockID);
            Courses course = _database.Courses.FirstOrDefault(x => x.CourseID == block.CourseID);
            Users creator = _database.Users.FirstOrDefault(x => x.UserID == course.UserID);

            Directory.CreateDirectory(Server.MapPath("~/Data/") + creator.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + lesson.LessonID + "/" + hw.HomeworkID + "/" + user.UserLogin);
            Directory.CreateDirectory(Server.MapPath("~/Data/") + creator.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + lesson.LessonID + "/" + hw.HomeworkID + "/" + user.UserLogin + "/TeacherAnswer/");
            string[] fileNames = Directory.GetFiles(Server.MapPath("~/Data/") + creator.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + lesson.LessonID + "/" + hw.HomeworkID + "/" + user.UserLogin);
            string[] teacherFileNames = Directory.GetFiles(Server.MapPath("~/Data/") + creator.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + lesson.LessonID + "/" + hw.HomeworkID + "/" + user.UserLogin + "/TeacherAnswer/");

            ViewBag.Files = fileNames;
            ViewBag.Answers = teacherFileNames;

            return View(hwut);
        }

        [HttpPost]
        public ActionResult CheckHWFullPage(UsersHomeworksTeachers h)
        {
            UsersHomeworksTeachers hwut = _database.UsersHomeworksTeachers.FirstOrDefault(x => x.ID == h.ID);

           
                if (h.Mark > 0)
                    hwut.Mark = h.Mark;
                else
                    hwut.Mark = null;

                hwut.TeacherComment = h.TeacherComment;

                _database.SaveChanges();
            

            return RedirectToAction("CheckHWFullPage", new { hwutID = hwut.ID });
        }

        public FileResult DownloadFile(string filePath)
        {
            FileInfo file = new FileInfo(filePath);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, file.Name);
        }

        public ActionResult DeleteFile(string filePath, int? hwutID)
        {
            System.IO.File.Delete(filePath);
            return RedirectToAction("CheckHWFullPage", new { hwutID = hwutID }); ;
        }

        [HttpPost]
        public ActionResult UploadFile(int? ID)
        {
            UsersHomeworksTeachers hwut = _database.UsersHomeworksTeachers.FirstOrDefault(x => x.ID == ID);
            Users user = _database.Users.FirstOrDefault(x => x.UserID == hwut.UserID);
            Homeworks hw = _database.Homeworks.FirstOrDefault(x => x.HomeworkID == hwut.HomeworkID);
            Lessons lesson = _database.Lessons.FirstOrDefault(x => x.LessonID == hw.LessonID);
            Blocks block = _database.Blocks.FirstOrDefault(x => x.BlockID == lesson.BlockID);
            Courses course = _database.Courses.FirstOrDefault(x => x.CourseID == block.CourseID);
            Users creator = _database.Users.FirstOrDefault(x => x.UserID == course.UserID);

            foreach (string s in Request.Files)
            {
                HttpPostedFileBase file = Request.Files[s];
                //  int fileSizeInBytes = file.ContentLength;
                string fileName = file.FileName;
                string fileExtension = file.ContentType;

                if (!string.IsNullOrEmpty(fileName))
                {
                    fileExtension = Path.GetExtension(fileName);
                    Directory.CreateDirectory(Server.MapPath("~/Data/") + creator.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + lesson.LessonID + "/" + hw.HomeworkID + "/" + user.UserLogin + "/TeacherAnswer/");
                    string pathToSave_100 = Server.MapPath("~/Data/") + creator.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + lesson.LessonID + "/" + hw.HomeworkID + "/" + user.UserLogin + "/TeacherAnswer/" + file.FileName;
                    file.SaveAs(pathToSave_100);
                }

            }

            return RedirectToAction("CheckHWFullPage", new { hwutID = ID });
            
        }

        [HttpGet]
        public ActionResult CreateCourse()
        {
            Users u = _database.Users.FirstOrDefault(x => x.UserLogin == User.Identity.Name);
            ViewBag.OrganizationID = u.UserID;
            return View();
        }

        [HttpPost]
        public ActionResult CreateCourse(Courses course)
        {
            Users u = _database.Users.FirstOrDefault(x => x.UserLogin == User.Identity.Name);

            
                course.CourseImage = "/Images/people.png";
                course.CourseBackground = "#337ab7";
                course.UserID = u.UserID;
            u.MyCourses.Add(course);
                _database.SaveChanges();
                Directory.CreateDirectory(Server.MapPath("~/Data/") + u.UserLogin + "/" + course.CourseID);
                return RedirectToAction("EditCourse", new { id = course.CourseID });
            

            return View(course);
        }

        [HttpGet]
        public ActionResult EditCourse(int? id)
        {

            Courses course = _database.Courses.FirstOrDefault(x => x.CourseID == id);

            return View(course);
        }

        [HttpPost]
        public ActionResult EditCourse(Courses c)
        {

            Courses course = _database.Courses.FirstOrDefault(x => x.CourseID == c.CourseID);

            if (ModelState.IsValid)
            {
                course.CourseName = c.CourseName;
                course.CourseDescription = c.CourseDescription;
                course.CoursePrice = c.CoursePrice;
                course.CourseDiscountPrice = c.CourseDiscountPrice;
                course.CourseDiscountDeadline = c.CourseDiscountDeadline;
                course.CourseBackground = c.CourseBackground;

                _database.SaveChanges();

                return View(course);
            }

            return View(c);
        }

        public ActionResult DeleteCourse(int? id)
        {

            Users u = _database.Users.FirstOrDefault(x => x.UserLogin == User.Identity.Name);
            Courses course = _database.Courses.FirstOrDefault(x => x.CourseID == id);



            foreach (Blocks bl in course.Blocks)
            {
                foreach (Lessons le in bl.Lessons)
                {

                    foreach (Homeworks hw in le.Homeworks)
                    {

                        hw.UsersHomeworksTeachers.Clear();

                    }
                    _database.Homeworks.RemoveRange(le.Homeworks);


                }
                _database.Lessons.RemoveRange(bl.Lessons);


            }

            _database.Blocks.RemoveRange(course.Blocks);

            course.Users.Clear();
            _database.Courses.Remove(course);

            _database.SaveChanges();
            DeleteDirectory((Server.MapPath("~/Data/") + u.UserLogin + "/" + course.CourseID));

            return RedirectToAction("Index");
        }



        [HttpPost]
        public JsonResult IsCourseExists(string CourseName)
        {


            return Json(!_database.Courses.Any(x => x.CourseName == CourseName), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CreateBlock(int? courseID)
        {
            ViewBag.CourseID = courseID;
            return View();
        }

        [HttpPost]
        public ActionResult CreateBlock(Blocks block)
        {
            Users u = _database.Users.FirstOrDefault(x => x.UserLogin == User.Identity.Name);
            Courses course = u.MyCourses.FirstOrDefault(x => x.CourseID == block.CourseID);

            if (ModelState.IsValid)
            {
                block.NumOfHours = 0;
                block.NumOfLessons = 0;
                course.Blocks.Add(block);
                _database.SaveChanges();
                Directory.CreateDirectory(Server.MapPath("~/Data/") + u.UserLogin + "/" + course.CourseID + "/" + block.BlockID);
                return RedirectToAction("EditBlockFullPage", new { id = block.BlockID, courseID = course.CourseID });
            }

            return View(block);
        }

        [HttpGet]
        public ActionResult EditBlock(int? id)
        {
            Blocks block = _database.Blocks.FirstOrDefault(x => x.BlockID == id);

            return View(block);
        }

        [HttpPost]
        public ActionResult EditBlock(Blocks b)
        {
            Blocks block = _database.Blocks.FirstOrDefault(x => x.BlockID == b.BlockID);
            if (ModelState.IsValid)
            {
                block.BlockName = b.BlockName;
                block.BlockDescription = b.BlockDescription;

                _database.SaveChanges();

                return RedirectToAction("EditCourse", new { id = block.CourseID });
            }

            return View(b);
        }

        [HttpGet]
        public ActionResult EditBlockFullPage(int? id)
        {

            Blocks block = _database.Blocks.FirstOrDefault(x => x.BlockID == id);




            return View(block);
        }

        public ActionResult DeleteBlock(int? id)
        {
            Users u = _database.Users.FirstOrDefault(x => x.UserLogin == User.Identity.Name);
            Blocks block = _database.Blocks.FirstOrDefault(x => x.BlockID == id);
            Courses course = _database.Courses.FirstOrDefault(x => x.CourseID == block.CourseID);

            foreach (Lessons le in block.Lessons)
            {

                foreach (Homeworks hw in le.Homeworks)
                {


                    hw.UsersHomeworksTeachers.Clear();

                }
                _database.Homeworks.RemoveRange(le.Homeworks);

            }
            _database.Lessons.RemoveRange(block.Lessons);


            _database.Lessons.RemoveRange(block.Lessons);

            _database.Blocks.Remove(block);

            _database.SaveChanges();

            DeleteDirectory((Server.MapPath("~/Data/") + u.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/"));

            return RedirectToAction("EditCourse", new { id = block.CourseID });
        }

        [HttpGet]
        public ActionResult CreateLesson(int? blockID)
        {
            ViewBag.BlockID = blockID;
            return View();
        }



        [HttpPost]
        public ActionResult CreateLesson(Lessons lesson)
        {

            Users u = _database.Users.FirstOrDefault(x => x.UserLogin == User.Identity.Name);

            Blocks block = _database.Blocks.FirstOrDefault(x => x.BlockID == lesson.BlockID);
            Courses course = u.MyCourses.FirstOrDefault(x => x.CourseID == block.CourseID);

            if (ModelState.IsValid)
            {
                lesson.LessonImage = "Images/people.png";
                block.Lessons.Add(lesson);

                block.NumOfLessons = block.Lessons.Count;
                block.NumOfHours = 0;
                foreach (Lessons l in block.Lessons)
                    block.NumOfHours += l.LessonDuration.Hours + (l.LessonDuration.Minutes / 60.0);

                _database.SaveChanges();
                Directory.CreateDirectory(Server.MapPath("~/Data/") + u.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + lesson.LessonID);
                return RedirectToAction("EditLessonFullPage", new { id = lesson.LessonID });
            }

            return View(block);
        }

        [HttpGet]
        public ActionResult EditLesson(int? id)
        {
            Lessons lesson = _database.Lessons.FirstOrDefault(x => x.LessonID == id);

            return View(lesson);
        }

        [HttpPost]
        public ActionResult EditLesson(Lessons l)
        {
            Lessons lesson = _database.Lessons.FirstOrDefault(x => x.LessonID == l.LessonID);
            Blocks block = _database.Blocks.FirstOrDefault(x => x.BlockID == l.BlockID);
            if (ModelState.IsValid)
            {
                lesson.LessonName = l.LessonName;
                lesson.LessonLecture = l.LessonLecture;
                lesson.LessonDuration = l.LessonDuration;
                lesson.LessonDescription = l.LessonDescription;



                block.NumOfLessons = block.Lessons.Count;
                block.NumOfHours = 0;
                foreach (Lessons le in block.Lessons)
                    block.NumOfHours += le.LessonDuration.Hours + (le.LessonDuration.Minutes / 60.0);

                _database.SaveChanges();

                return RedirectToAction("EditBlockFullPage", new { id = block.BlockID });
            }

            return View(l);
        }

        [HttpGet]
        public ActionResult EditLessonFullPage(int? id)
        {

            Lessons lesson = _database.Lessons.FirstOrDefault(x => x.LessonID == id);
            Blocks block = _database.Blocks.FirstOrDefault(x => x.BlockID == lesson.BlockID);
            Courses course = _database.Courses.FirstOrDefault(x => x.CourseID == block.CourseID);
            Users u = _database.Users.FirstOrDefault(x => x.UserLogin == User.Identity.Name);

            Directory.CreateDirectory(Server.MapPath("~/Data/") + u.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + lesson.LessonID + "/LessonData/");
            string[] fileNames = Directory.GetFiles(Server.MapPath("~/Data/") + u.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + lesson.LessonID + "/LessonData/");


            ViewBag.Files = fileNames;





            return View(lesson);
        }

        public ActionResult DeleteLesson(int? id)
        {

            Lessons lesson = _database.Lessons.FirstOrDefault(x => x.LessonID == id);
            Users u = _database.Users.FirstOrDefault(x => x.UserLogin == User.Identity.Name);
            Blocks block = _database.Blocks.FirstOrDefault(x => x.BlockID == lesson.BlockID);
            Courses course = _database.Courses.FirstOrDefault(x => x.CourseID == block.CourseID);


            foreach (Homeworks hw in lesson.Homeworks)
            {


                hw.UsersHomeworksTeachers.Clear();

            }
            _database.Homeworks.RemoveRange(lesson.Homeworks);



            _database.Lessons.Remove(lesson);

            block.NumOfLessons--;
            block.NumOfHours -= lesson.LessonDuration.Hours + (lesson.LessonDuration.Minutes / 60);

            _database.SaveChanges();

            DeleteDirectory((Server.MapPath("~/Data/") + u.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + lesson.LessonID + "/"));

            return RedirectToAction("EditBlock", new { id = lesson.BlockID });
        }

        [HttpGet]
        public ActionResult CreateHW(int? lessonID)
        {
            ViewBag.LessonID = lessonID;
            return View();
        }



        [HttpPost]
        public ActionResult CreateHW(Homeworks hw)
        {
            Users creator = _database.Users.FirstOrDefault(x => x.UserLogin == User.Identity.Name);
            Lessons lesson = _database.Lessons.FirstOrDefault(x => x.LessonID == hw.LessonID);
            Blocks block = _database.Blocks.FirstOrDefault(x => x.BlockID == lesson.BlockID);
            Courses course = _database.Courses.FirstOrDefault(x => x.CourseID == block.CourseID);

            if (ModelState.IsValid)
            {

                lesson.Homeworks.Add(hw);
                foreach (Users u in course.Users)
                {
                    UsersHomeworksTeachers uht = new UsersHomeworksTeachers();
                    uht.HomeworkID = hw.HomeworkID;
                    uht.UserID = u.UserID;
                    u.UsersHomeworksTeachersU.Add(uht);
                }

                _database.SaveChanges();
                Directory.CreateDirectory(Server.MapPath("~/Data/") + creator.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + lesson.LessonID + "/" + hw.HomeworkID);
                return RedirectToAction("EditHWFullPage", new { id = hw.HomeworkID });
            }

            return View(hw);
        }

        [HttpGet]
        public ActionResult EditHW(int? id)
        {
            Homeworks hw = _database.Homeworks.FirstOrDefault(x => x.HomeworkID == id);

            return View(hw);
        }

        [HttpPost]
        public ActionResult EditHW(Homeworks h)
        {
            Homeworks hw = _database.Homeworks.FirstOrDefault(x => x.HomeworkID == h.HomeworkID);
            Lessons lesson = _database.Lessons.FirstOrDefault(x => x.LessonID == hw.LessonID);
            if (ModelState.IsValid)
            {
                hw.HomeworkName = h.HomeworkName;
                hw.HomeworkTask = h.HomeworkTask;
                hw.HomeworkGoal = h.HomeworkGoal;
                hw.HomeworkDeadline = h.HomeworkDeadline;




                _database.SaveChanges();

                return RedirectToAction("EditLessonFullPage", new { id = lesson.LessonID });
            }

            return View(h);
        }

        [HttpGet]
        public ActionResult EditHWFullPage(int? id)
        {

            Homeworks hw = _database.Homeworks.FirstOrDefault(x => x.HomeworkID == id);

            return View(hw);
        }

        public ActionResult DeleteHW(int? id)
        {

            Homeworks hw = _database.Homeworks.FirstOrDefault(x => x.HomeworkID == id);
            Users u = _database.Users.FirstOrDefault(x => x.UserLogin == User.Identity.Name);
            Lessons lesson = _database.Lessons.FirstOrDefault(x => x.LessonID == hw.LessonID);
            Blocks block = _database.Blocks.FirstOrDefault(x => x.BlockID == lesson.BlockID);
            Courses course = _database.Courses.FirstOrDefault(x => x.CourseID == block.CourseID);



            hw.UsersHomeworksTeachers.Clear();


            _database.Homeworks.Remove(hw);

            _database.SaveChanges();

            DeleteDirectory((Server.MapPath("~/Data/") + u.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + lesson.LessonID + "/" + hw.HomeworkID + "/"));

            return RedirectToAction("EditLessonFullPage", new { id = hw.LessonID });
        }









        public ActionResult ChangeCourseImage(string CourseID, HttpPostedFileBase upload)
        {

            Users u = _database.Users.FirstOrDefault(x => x.UserLogin == User.Identity.Name);
            Courses course = _database.Courses.FirstOrDefault(x => x.CourseID.ToString() == CourseID);

            if (upload != null)
            {

                if (!string.IsNullOrEmpty(upload.FileName))
                {
                    FileInfo fi = new FileInfo(upload.FileName);
                    if (fi.Extension.ToLower() == ".png" || fi.Extension.ToLower() == ".jpg" || fi.Extension.ToLower() == ".jpeg")
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Data/") + u.UserLogin + "/" + course.CourseID + "/Image/");

                        string[] teacherFileNames = Directory.GetFiles((Server.MapPath("~/Data/") + u.UserLogin + "/" + course.CourseID + "/Image/"));
                        foreach (string path in teacherFileNames)
                            System.IO.File.Delete(path);

                        string pathToSave_100 = Server.MapPath("~/Data/") + u.UserLogin + "/" + course.CourseID + "/Image/" + upload.FileName;
                        upload.SaveAs(pathToSave_100);

                        pathToSave_100 = "/Data/" + u.UserLogin + "/" + course.CourseID + "/Image/" + upload.FileName;

                        course.CourseImage = pathToSave_100;
                        _database.SaveChanges();
                    }


                }


            }

            return RedirectToAction("EditCourse", new { id = course.CourseID });
        }

        public ActionResult DeleteCourseImage(string CourseID)
        {
            Courses course = _database.Courses.FirstOrDefault(x => x.CourseID.ToString() == CourseID);
            Users u = _database.Users.FirstOrDefault(x => x.UserLogin == User.Identity.Name);
            course.CourseImage = "/Images/people.png";

            Directory.CreateDirectory(Server.MapPath("~/Data/") + u.UserLogin + "/" + course.CourseID + "/Image/");

            string[] teacherFileNames = Directory.GetFiles((Server.MapPath("~/Data/") + u.UserLogin + "/" + course.CourseID + "/Image/"));
            foreach (string path in teacherFileNames)
                System.IO.File.Delete(path);

            _database.SaveChanges();

            return RedirectToAction("EditCourse", new { id = course.CourseID });
        }

        public ActionResult ChangeLessonImage(string LessonID, HttpPostedFileBase upload)
        {

            Users u = _database.Users.FirstOrDefault(x => x.UserLogin == User.Identity.Name);
            Lessons lesson = _database.Lessons.FirstOrDefault(x => x.LessonID.ToString() == LessonID);
            Blocks block = _database.Blocks.FirstOrDefault(x => x.BlockID == lesson.BlockID);
            Courses course = _database.Courses.FirstOrDefault(x => x.CourseID == block.CourseID);

            if (upload != null)
            {

                if (!string.IsNullOrEmpty(upload.FileName))
                {
                    FileInfo fi = new FileInfo(upload.FileName);
                    if (fi.Extension.ToLower() == ".png" || fi.Extension.ToLower() == ".jpg" || fi.Extension.ToLower() == ".jpeg")
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Data/") + u.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + lesson.LessonID + "/Image/");

                        string[] teacherFileNames = Directory.GetFiles((Server.MapPath("~/Data/") + u.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + lesson.LessonID + "/Image/"));
                        foreach (string path in teacherFileNames)
                            System.IO.File.Delete(path);

                        string pathToSave_100 = Server.MapPath("~/Data/") + u.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + lesson.LessonID + "/Image/" + upload.FileName;
                        upload.SaveAs(pathToSave_100);

                        pathToSave_100 = "/Data/" + u.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + lesson.LessonID + "/Image/" + upload.FileName;

                        lesson.LessonImage = pathToSave_100;
                        _database.SaveChanges();
                    }


                }


            }

            return RedirectToAction("EditLessonFullPage", new { id = lesson.LessonID });
        }

        public ActionResult DeleteLessonImage(string LessonID)
        {
            Lessons lesson = _database.Lessons.FirstOrDefault(x => x.LessonID.ToString() == LessonID);
            Blocks block = _database.Blocks.FirstOrDefault(x => x.BlockID == lesson.BlockID);
            Courses course = _database.Courses.FirstOrDefault(x => x.CourseID == block.CourseID);
            Users u = _database.Users.FirstOrDefault(x => x.UserLogin == User.Identity.Name);
            lesson.LessonImage = "/Images/people.png";

            string[] teacherFileNames = Directory.GetFiles((Server.MapPath("~/Data/") + u.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + lesson.LessonID + "/Image/"));
            foreach (string path in teacherFileNames)
                System.IO.File.Delete(path);

            _database.SaveChanges();

            return RedirectToAction("EditLessonFullPage", new { id = lesson.LessonID });
        }

        private void DeleteDirectory(string target_dir)
        {
            string[] files = Directory.GetFiles(target_dir);
            string[] dirs = Directory.GetDirectories(target_dir);

            foreach (string file in files)
            {
                System.IO.File.SetAttributes(file, FileAttributes.Normal);
                System.IO.File.Delete(file);


            }

            foreach (string dir in dirs)
            {
                DeleteDirectory(dir);
            }

            Directory.Delete(target_dir, false);
        }

        public ActionResult DeleteLessonFile(string filePath, int? id)
        {
            System.IO.File.Delete(filePath);
            return RedirectToAction("EditLessonFullPage", new { id = id }); ;
        }

        [HttpPost]
        public ActionResult UploadLessonFile(int? ID)
        {



            Lessons lesson = _database.Lessons.FirstOrDefault(x => x.LessonID == ID);
            Blocks block = _database.Blocks.FirstOrDefault(x => x.BlockID == lesson.BlockID);
            Courses course = _database.Courses.FirstOrDefault(x => x.CourseID == block.CourseID);
            Users u = _database.Users.FirstOrDefault(x => x.UserLogin == User.Identity.Name);

            foreach (string s in Request.Files)
            {
                HttpPostedFileBase file = Request.Files[s];
                //  int fileSizeInBytes = file.ContentLength;
                string fileName = file.FileName;
                string fileExtension = file.ContentType;

                if (!string.IsNullOrEmpty(fileName))
                {
                    fileExtension = Path.GetExtension(fileName);
                    Directory.CreateDirectory(Server.MapPath("~/Data/") + u.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + lesson.LessonID + "/LessonData/");
                    string pathToSave_100 = Server.MapPath("~/Data/") + u.UserLogin + "/" + course.CourseID + "/" + block.BlockID + "/" + lesson.LessonID + "/LessonData/" + file.FileName;
                    file.SaveAs(pathToSave_100);
                }

            }

            return RedirectToAction("EditLessonFullPage", new { id = ID }); ;

        }
    }
}