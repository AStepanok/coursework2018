﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineCourses.Models;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using System.IO;
using System.Security.Cryptography;
using System.Web.Security;

namespace OnlineCourses.Controllers
{
    public class UserManagerController : Controller
    {

        coursesDBEntities _database = new coursesDBEntities();  

        [HttpGet]
        public ActionResult Index()
        {
            Users u = _database.Users.FirstOrDefault(x => x.UserLogin == User.Identity.Name);
            Directory.CreateDirectory(Server.MapPath("~/Data/") + u.UserLogin);
            return View(u);
        }

        [HttpPost]
        public ActionResult Index(Users u)
        {

            Users user = _database.Users.FirstOrDefault(x => x.UserID == u.UserID);
            u.UserImage = user.UserImage;
            u.UserLogin = user.UserLogin;
            if (ModelState.IsValid)
            {
                if (u.UserEmail == user.UserEmail || CheckUserEmail(u.UserEmail))
                {
                    user.UserName = u.UserName;
                    user.UserEmail = u.UserEmail;
                    user.UserPhone = u.UserPhone;
                    user.UserVK = u.UserVK;

                    _database.SaveChanges();

                    return View(user);
                }
                else
                {
                    ModelState.AddModelError("EmailError", "Такой email уже привязан к другому аккаунту!");

                }
            }

            return View(u);
        }

        public bool CheckUserEmail(string email)
        {
            if (_database.Users.FirstOrDefault(x => x.UserEmail.ToLower() == email.ToLower()) == null 
               )
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void ChangePassword(int? u, string newPas, string newPasConfirm)
        {
            Users user = _database.Users.FirstOrDefault(x => x.UserID == u);

            if (newPas == "" && newPasConfirm == "")
            {
                ;
            }
            else if ((newPas == "" && newPasConfirm != "") || (newPas != "" && newPasConfirm != newPas)) ;
            else
            {
                var salt = System.Text.Encoding.UTF8.GetBytes("qazwsxedcrfv1029384756");
                var password = System.Text.Encoding.UTF8.GetBytes(newPas);

                var hmacMD5 = new HMACMD5(salt);
                var saltedHash = hmacMD5.ComputeHash(password);
                saltedHash = hmacMD5.ComputeHash(saltedHash);

                user.UserPassword = newPas;

                _database.SaveChanges();

            }




        }

        public ActionResult ChangeAvatar(HttpPostedFileBase upload)
        {
            Users user = _database.Users.FirstOrDefault(x => x.UserLogin == User.Identity.Name);

            if (upload != null)
            {

                if (!string.IsNullOrEmpty(upload.FileName))
                {
                    FileInfo fi = new FileInfo(upload.FileName);
                    if (fi.Extension.ToLower() == ".png" || fi.Extension.ToLower() == ".jpg" || fi.Extension.ToLower() == ".jpeg")
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Data/") + user.UserLogin + "/Users/" + user.UserLogin + "/");

                        string[] teacherFileNames = Directory.GetFiles((Server.MapPath("~/Data/") + user.UserLogin + "/Users/" + user.UserLogin + "/"));
                        foreach (string path in teacherFileNames)
                            System.IO.File.Delete(path);

                        string pathToSave_100 = Server.MapPath("~/Data/") + user.UserLogin + "/Users/" + user.UserLogin + "/" + upload.FileName;
                        upload.SaveAs(pathToSave_100);

                        pathToSave_100 = "/Data/" + user.UserLogin + "/Users/" + user.UserLogin + "/" + upload.FileName;

                        user.UserImage = pathToSave_100;
                        _database.SaveChanges();
                    }


                }


            }

            return RedirectToAction("Index");
        }

        public ActionResult DeleteAvatar()
        {
            Users user = _database.Users.FirstOrDefault(x => x.UserLogin == User.Identity.Name);

            user.UserImage = "/Images/people.png";
            Directory.CreateDirectory(Server.MapPath("~/Data/") + user.UserLogin + "/Users/" + user.UserLogin + "/");
            string[] teacherFileNames = Directory.GetFiles((Server.MapPath("~/Data/") + user.UserLogin + "/Users/" + user.UserLogin + "/"));
            foreach (string path in teacherFileNames)
                System.IO.File.Delete(path);

            _database.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Home");
        }

        

       

       
    }
}