﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineCourses.Models;
using System.Web.Security;
using System.Net.Mail;
using System.Net;
using System.Text.RegularExpressions;
using System.Text;
using System.Security.Cryptography;

namespace OnlineCourses.Controllers
{
    public class HomeController : Controller
    {
        coursesDBEntities _database = new coursesDBEntities();
        


        // GET: Home
        public ActionResult Index(int? id)
        {
            
                return RedirectToAction("Login");
            
                
        }

        [HttpGet]
        public ActionResult Login()
        {
            
            if (User.Identity.IsAuthenticated)
            {
                
                    return RedirectToAction("Index", "UserManager");
                
            }
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginPassword u)
        {
            if (ModelState.IsValid)
            {
                var salt = System.Text.Encoding.UTF8.GetBytes("qazwsxedcrfv1029384756");
                var hmacMD5 = new HMACMD5(salt);
                var password = Encoding.UTF8.GetBytes(u.UserPassword);
                var saltedHash = hmacMD5.ComputeHash(password);
                
                saltedHash = hmacMD5.ComputeHash(saltedHash);

                
                        Users _user = _database.Users.Where(x => (x.UserLogin.ToLower() == u.UserLogin.ToLower() || x.UserEmail.ToLower() == u.UserLogin.ToLower()) 
                        && x.UserPassword == u.UserPassword).FirstOrDefault();
                        if (_user == null)
                            ModelState.AddModelError("LoginError", "Неверный логин и/или пароль!");
                        else
                        {

                            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                            10,
                            _user.UserLogin,
                            DateTime.Now,
                            DateTime.Now.AddMinutes(40),
                            u.Remember,
                            "user"
                            );


                            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(authTicket));
                            if (u.Remember)
                                cookie.Expires = authTicket.Expiration;
                            Response.Cookies.Add(cookie);

                            return RedirectToAction("Index", "UserManager");
                        }
                    
                    
                        
            }           
            return View();
        }

        [HttpPost]
        public ActionResult CheckUserLogin(string login)
        {
            if (_database.Users.FirstOrDefault(x => x.UserLogin.ToLower() == login.ToLower()) == null  && Regex.IsMatch(login, @"^[a-zA-Z0-9]+$"))
            {
                return Json(new { success = true}, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult CheckUserEmail(string email)
        {
            if (_database.Users.FirstOrDefault(x => x.UserEmail.ToLower() == email.ToLower()) == null
               )
            {
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public void Register(string FIO, string login, string email, string pas, string orgName) 
        {
            

            Users user = new Users();
            user.UserName = FIO;
            user.UserLogin = login;
            
            user.UserEmail = email;
            user.UserImage = "/Images/people.png";
            

            var salt = System.Text.Encoding.UTF8.GetBytes("qazwsxedcrfv1029384756");
            var password = System.Text.Encoding.UTF8.GetBytes(pas);

            var hmacMD5 = new HMACMD5(salt);
            var saltedHash = hmacMD5.ComputeHash(password);
            saltedHash = hmacMD5.ComputeHash(saltedHash);

            user.UserPassword = pas;


            _database.Users.Add(user);

            _database.SaveChanges();
        }

        [HttpGet]
        public ActionResult ResetPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ResetPassword(LoginPassword data)
        {
            if (data.UserEmail != null && (_database.Users.FirstOrDefault(x => x.UserEmail.ToLower() == data.UserEmail.ToLower()) != null)
                )
            {
                string newPas;
                int length = 10;
                const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
                StringBuilder res = new StringBuilder();
                using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
                {
                    byte[] uintBuffer = new byte[sizeof(uint)];

                    while (length-- > 0)
                    {
                        rng.GetBytes(uintBuffer);
                        uint num = BitConverter.ToUInt32(uintBuffer, 0);
                        res.Append(valid[(int)(num % (uint)valid.Length)]);
                    }

                    newPas = res.ToString();


                }
                var salt = System.Text.Encoding.UTF8.GetBytes("qazwsxedcrfv1029384756");
                var password = System.Text.Encoding.UTF8.GetBytes(newPas);

                var hmacMD5 = new HMACMD5(salt);
                var saltedHash = hmacMD5.ComputeHash(password);
                saltedHash = hmacMD5.ComputeHash(saltedHash);
                if (_database.Users.FirstOrDefault(x => x.UserEmail.ToLower() == data.UserEmail.ToLower()) != null)
                {
                    Users u = _database.Users.FirstOrDefault(x => x.UserEmail.ToLower() == data.UserEmail.ToLower());
                    

                    u.UserPassword = newPas;



                }
                
                _database.SaveChanges();

                var message = new MailMessage();
                message.To.Add(new MailAddress(data.UserEmail));  // replace with valid value 
                message.From = new MailAddress("anton.stepanok@gmail.com");  // replace with valid value
                message.Subject = "Новый пароль";
                message.Body = "Ваш новый пароль: " + newPas;
                message.IsBodyHtml = true;

                using (var smtp = new SmtpClient())
                {
                    var credential = new NetworkCredential
                    {
                        UserName = "anton.stepanok@gmail.com",  // replace with valid value
                        Password = "fynjyltdtkjgth1997"  // replace with valid value
                    };
                    smtp.Credentials = credential;
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.Send(message);

                }
            } else
            {
                ModelState.AddModelError("EmailError", "Пользователя с таким email не существует!");
                return View(data);
            }
            return RedirectToAction("Login");
        }
    }
}